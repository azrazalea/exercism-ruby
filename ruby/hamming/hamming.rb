class Hiamming
  class << self
    def compute(strand_1, strand_2)
      raise ArgumentError unless strand_1.length == strand_2.length
      return 0 if strand_1 == strand_2 # Just a shortcut for optimization

      self.compare_strands(strand_1, strand_2).length
    end

    def compare_strands(strand_1, strand_2)
      counter = 0

      strand_1.split("").reject do |item|
        cmp = item == strand_2[counter]
        counter += 1
        cmp
      end
    end
  end
end

module BookKeeping
  VERSION = 3
end
