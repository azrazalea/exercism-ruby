class Gigasecond
  GIGASECOND = 10**9

  def self.from(birth_time)
    birth_time + GIGASECOND
  end
end

module BookKeeping
  VERSION = 6
end
